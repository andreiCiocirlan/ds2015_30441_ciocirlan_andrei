<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Main Page</title>

<link rel="stylesheet" type="text/css" href="css/menu.css">
</head>
<body>
	
	<br>
	<h3>
		Hello,
		<c:out value="${sessionScope.user.firstname} ${sessionScope.user.lastname} "></c:out>

		<iframe
			src="http://free.timeanddate.com/clock/i4rfpygi/n3686/tlro32/fn6/fs20/fcfff/tct/pct/ftb/pa8/tt0/tw1/th1/ta1/tb4"
			frameborder="0" width="254" height="64"></iframe>
		&nbsp;
	</h3>

	<br>
	<form action="LogoutServlet.do">
		<input type="submit" class="styled-button-1" value="Logout"
			style="margin-right: 60px" />
	</form>

	<br>
		<ul class="nav">
			<c:if test="${sessionScope.userType == 'client'}">
				<li><a href="#">Flights</a>
					<ul>
						<li><a href="localTimeServlet.do">View Local Time for Flight</a></li>
						<li><a href="clientViewFlightsServlet.do">View Flights</a></li>
					</ul>
				</li>
			</c:if>
			<li><a href="#">Informatii</a>
				<ul>
					<li><a href="transferServlet.do">Contact</a></li>
					<li><a href="alimentareContServlet.do">Arhiva Imagini</a></li>
				</ul>
			</li>
			<c:if test="${sessionScope.userType == 'admin'}">
				<li><a href="#">CRUD Operations</a>
					<ul>
						<li><a href="adminViewFlightsServlet.do">View All Flights</a></li>
						<li><a href="insertFlightsServlet.do">Schedule new Flight</a></li>
						<li><a href="updateFlightServlet.do">Update Flight Information</a></li>
						<li><a href="deleteFlightsServlet.do">Delete Flight</a></li>
					</ul>
				</li>
			</c:if>
		</ul>
		
</body>
</html>