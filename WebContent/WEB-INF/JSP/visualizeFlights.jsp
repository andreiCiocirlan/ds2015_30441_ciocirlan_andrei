<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<link rel="stylesheet" type="text/css" href="css/section.css">
<link rel="stylesheet" type="text/css" href="css/table.css">
<link rel="stylesheet" type="text/css" href="css/menu.css">

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>View Flights</title>
</head>
<body>
	<%@include file="include/mainHeader.jsp"%>
	<br>
	<br>
	<br>
	<br>
	<div class="section">
		<div class="wrapper">
			<h2>View Flights</h2>
			<br>

			<p style="text-align: center; color: white;">Your Scheduled Flights</p>

			<br>
			<table>
				<tr>
					<th>Flight Number</th>
					<th>Airplane Type</th>
					<th>Departure City</th>
					<th>Arrival City</th>
					<th>Departure Date</th>
					<th>Arrival Date</th>
					<th>Departure Hour</th>
					<th>Arrival Hour</th>
				</tr>
				<c:forEach var="flight" items="${requestScope.flights}">
					<tr>
						<td><c:out value="${flight.flightNumber}" /></td>
						<td><c:out value="${flight.airplaneType}" /></td>
						<td><c:out value="${flight.departureCityName}" /></td>
						<td><c:out value="${flight.arrivalCityName}" /></td>
						<td><c:out value="${flight.departureDate}" /></td>
						<td><c:out value="${flight.arrivalDate}" /></td>
						<td><c:out value="${flight.departureHour}" /></td>
						<td><c:out value="${flight.arrivalHour}" /></td>
					</tr>
				</c:forEach>
			</table>
			<br>
			<br>
			<br>
			<p style="text-align: center; color: white">
				<c:out value="${requestScope.message}"></c:out>
			</p>
			<div class="push"></div>
		</div>
	</div>
	<div id="footer"><%@include file="include/footer.jsp"%></div>

</body>
</html>