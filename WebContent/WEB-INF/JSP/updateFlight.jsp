<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<link rel="stylesheet"
	href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<link rel="stylesheet" type="text/css" href="css/section.css">
<link rel="stylesheet" type="text/css" href="css/table.css">
<link rel="stylesheet" type="text/css" href="css/menu.css">
<style type="text/css">
.ui-widget { font-size: 11px; }
</style>
<script>
	$(function() {
		$("#datepicker1").datepicker();
	});
</script>
<script>
	$(function() {
		$("#datepicker2").datepicker();
	});

	$("#datepicker1").datepicker({
		onSelect : function(dateText) {
			startDate = JSON.stringify(dateText);
		}
	});

	$("#datepicker2").datepicker({
		onSelect : function(dateText) {
			endDate = JSON.stringify(dateText);
		}
	});
</script>


<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Update Flight</title>
</head>
<body>
	<%@include file="include/mainHeader.jsp"%>
	<br>
	<br>
	<br>
	<br>
	<div class="section">
		<h2>Update Flight</h2>
		<br>
			<table>
				<tr>
					<th>Flight Number</th>
					<th>Airplane Type</th>
					<th>Departure City</th>
					<th>Arrival City</th>
					<th>Departure Date</th>
					<th>Arrival Date</th>
					<th>Departure Hour</th>
					<th>Arrival Hour</th>
				</tr>
				<c:forEach var="flight" items="${requestScope.flights}">
					<tr>
						<td><c:out value="${flight.flightNumber}" /></td>
						<td><c:out value="${flight.airplaneType}" /></td>
						<td><c:out value="${flight.departureCityName}" /></td>
						<td><c:out value="${flight.arrivalCityName}" /></td>
						<td><c:out value="${flight.departureDate}" /></td>
						<td><c:out value="${flight.arrivalDate}" /></td>
						<td><c:out value="${flight.departureHour}" /></td>
						<td><c:out value="${flight.arrivalHour}" /></td>
					</tr>
				</c:forEach>
			</table>
		<br>
		<form action="updateFlightServlet.do" method="post">
			<p style="text-align: center; color: white;">Update Flight
				information !</p>

			<br>
			<p style="text-align: center; color: white">
				Flight number: <input type="text" name="flightNumber">
			</p>
			<br>
			<p style="text-align: center; color: white">
				Airplane type: <input type="text" name="airplaneType">
			</p>
			<br>
			<p style="text-align: center; color: white">
				User ID: <input type="text" name="userID">
			</p>
			<br>

			<p style="text-align: center; color: white">
				Departure City: <select name="departureCity"
					id="departureCitySelector">
					<option value="default">Select the departure city</option>
					<c:forEach var="city" items="${requestScope.cities}">
						<option value="${city.cityID}">${city.cityName}</option>
					</c:forEach>
				</select>
			</p>
			<br>
			<p style="text-align: center; color: white">
				Arrival City: <select name="arrivalCity" id="arrivalCitySelector">
					<option value="default">Select the arrival city</option>
					<c:forEach var="city" items="${requestScope.cities}">
						<option value="${city.cityID}">${city.cityName}</option>
					</c:forEach>
				</select>

			</p>
			<br>
			<p style="text-align: center; color: white">
				Departure Hour: <input type="text" name="departureHour">
			</p>
			<br>
			<p style="text-align: center; color: white">
				Arrival Hour: <input type="text" name="arrivalHour">
			</p>
			<br>
			
				<p style="text-align: center; color: white">
					Departure Date:<input type="text" name="departureDate" id="datepicker1"
						maxlength="11" size="11" style="padding-left: 30px">
				</p>
				<br>
				<p style="text-align: center; color: white">
					Arrival Date:<input type="text" name="arrivalDate" id="datepicker2"
						maxlength="11" size="11" style="padding-left: 30px">
				</p>
			
			<br> <input type="submit" class="btn" value="Update Flight"
				style="bottom: 10px; position: relative; left: 40%;">
		</form>
		<p style="text-align: center; color: white">
			<c:out value="${requestScope.message}"></c:out>
		</p>
		<div class="push"></div>
	</div>


	<div class="footer"><%@include file="include/footer.jsp"%></div>

</body>
</html>