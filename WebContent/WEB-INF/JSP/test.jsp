<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script src="//code.jquery.com/jquery-2.1.1.min.js"></script>
  <title>Mashape Query</title>
  <script>
	function doIt() { 
 var output = $.ajax({
    url: 'https://maps.googleapis.com/maps/api/timezone/json?location=39.6034810,-119.6822510&timestamp=1331161200&key=AIzaSyCvj4nLdP1bbQyJIKdE4oGxxN7fBPyq2gc', // The URL to the API. You can get this by clicking on "Show CURL example" from an API profile
    type: 'GET', // The HTTP Method, can be GET POST PUT DELETE etc
    data: {}, // Additional parameters here
    dataType: 'json',
    success: function(data) {
    	//
        //Change data.source to data.something , where something is whichever part of the object you want returned.
        //To see the whole object you can output it to your browser console using:
        console.log(data.rawOffset);
       	document.getElementById("output").innerHTML = data.rawOffset; 
        },
    error: function(err) { alert(err); },
   // beforeSend: function(xhr) {
   // xhr.setRequestHeader("X-Mashape-Authorization", "fvnZiHRhXNmshi7kg9kI9Gg4uGLYp1SJPSCjsn1HvBBGZPGmAz"); // Enter here your Mashape key
   // }
});
  

}

</script>
</head>
<body>

  <button onclick="doIt()">Run the request</button>
  <div id="output">The API request is:</div>
  
</body>
</html>