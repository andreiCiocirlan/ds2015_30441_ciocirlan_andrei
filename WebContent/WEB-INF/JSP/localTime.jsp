<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>

<link rel="stylesheet" type="text/css" href="css/section.css">
<link rel="stylesheet" type="text/css" href="css/menu.css">
<script src="//code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="javascript/time.js"></script>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Local Time</title>
</head>
<body>
	<%@include file="include/mainHeader.jsp"%>
	<br>
	<br>
	<br>
	<br>
	<div class="section">
		<div class="wrapper">
		<h2>Get Local Time</h2>
		<br>
		<form action="localTimeServlet.do" method="get">
			<p style="text-align: center; color: white">
					City: <select name="cityLocalTime"
						id="citySelector">
						<option value="default">Select the departure city</option>
						<c:forEach var="city" items="${requestScope.cities}">
							<option value2="${city.cityLatitude}" value="${city.cityLongitude}">${city.cityName}</option>
								
						</c:forEach>
					</select>
				</p>
				<br>
				<p style="text-align: center; color: white">
					<input type='button' value="Get Local Time" class="btn" onClick='doIt()'>
				</p>
  		</form>		
  			
  				<br>
  				<div id="output" style="text-align: center; color: white">
  					
  				</div>
		</div>
		<div class="push"></div>
	</div>
 

	<div id="footer"><%@include file="include/footer.jsp"%></div>

</body>
</html>