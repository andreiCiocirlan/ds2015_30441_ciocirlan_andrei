var cityLongitude;
var cityLatitude;
var offset;

function doIt() { 
	 var output = $.ajax({
	    url: "https://maps.googleapis.com/maps/api/timezone/json?location="+cityLatitude+","+ cityLongitude+"&timestamp=1445804131&key=AIzaSyCvj4nLdP1bbQyJIKdE4oGxxN7fBPyq2gc", // The URL to the API. You can get this by clicking on "Show CURL example" from an API profile
	    type: 'GET', // The HTTP Method, can be GET POST PUT DELETE etc
	    data: {}, // Additional parameters here
	    dataType: 'json',
	    success: function(data) {
	    	//
	        //Change data.source to data.something , where something is whichever part of the object you want returned.
	        //To see the whole object you can output it to your browser console using:
	        console.log(data.rawOffset);
	        console.log(data.dstOffset);
	        offset = data.rawOffset;
	    	offset += data.dstOffset;
	        calcTime(offset);
	       //	document.getElementById("output").innerHTML = data.rawOffset; 
	        },
	    error: function(err) { alert(err); },
	   // beforeSend: function(xhr) {
	   // xhr.setRequestHeader("X-Mashape-Authorization", "fvnZiHRhXNmshi7kg9kI9Gg4uGLYp1SJPSCjsn1HvBBGZPGmAz"); // Enter here your Mashape key
	   // }
	});
	  

	}

function calcTime(offset) {
    var d = new Date();
    var utc = d.getTime() + (d.getTimezoneOffset() * 60000);
    var nd = new Date(utc + (1000*offset));

    document.getElementById("output").innerHTML =
        "The local time is " + nd.toLocaleString();
}

$(document).ready(function() {
	
        $("#citySelector").change(function(){
        	cityLongitude = $(this).val();
        	var element = $("option:selected", this);
            cityLatitude = element.attr("value2");
            console.log(cityLongitude);
            console.log(cityLatitude);
        });
        
});

