<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<link rel="stylesheet" type="text/css" href="css/login.css">

</head>
<body>
	<div id="login">
		<h1>
			<strong>Welcome to the ROAirlines!</strong> <br>Please enter your credentials.
		</h1>
		<form action="loginServlet" method="post">
			<fieldset>
				<p>
					Username:<input type="text" name="username" >
				</p>
				<p>
					Password: <input type="password" name="password" >
				</p>

				<p>
					<input type="submit" value="login">
				</p>
			</fieldset>
		</form>
		<c:if test="${not empty message}">
   			<c:out value="${message}"></c:out>
		</c:if>
		
	</div>
</body>
</html>