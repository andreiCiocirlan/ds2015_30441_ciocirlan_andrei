package airport.forms;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import airport.exception.ValidationException;
import airport.model.Flight;

public class FlightForm {

	private String flightNumberAsString;
	private String userIDAsString;
	private String departureCityAsString;
	private String arrivalCityAsString;
	private String arrivalHourAsString;
	private String departureHourAsString;
	private String airplaneTypeAsString;
	private String departureDateAsString;
	private String arrivalDateAsString;

	public static FlightForm fromRequest(HttpServletRequest request) {

		FlightForm form = new FlightForm();

		form.flightNumberAsString = request.getParameter("flightNumber");
		form.userIDAsString = request.getParameter("userID");
		form.airplaneTypeAsString = request.getParameter("airplaneType");
		form.departureCityAsString = request.getParameter("departureCity");
		form.arrivalCityAsString = request.getParameter("arrivalCity");
		form.arrivalHourAsString = request.getParameter("arrivalHour");
		form.departureHourAsString = request.getParameter("departureHour");
		form.departureDateAsString = request.getParameter("departureDate");
		form.arrivalDateAsString = request.getParameter("arrivalDate");
		
		return form;
	}

	public Flight toModel() throws ValidationException {

		Flight flight = new Flight();

		int userID = -1;
		int flightNumber = -1;
		String airplaneType = null;
		int departureCity = -1;
		int arrivalCity = -1;
		String departureHour = null;
		String arrivalHour = null;

		if (!userIDAsString.isEmpty() || !flightNumberAsString.isEmpty() || !userIDAsString.isEmpty()
				|| !airplaneTypeAsString.isEmpty() || !departureCityAsString.isEmpty() || !arrivalCityAsString.isEmpty()
				|| !departureHourAsString.isEmpty() || !arrivalHourAsString.isEmpty()) {
			try {
				userID = Integer.parseInt(userIDAsString);
				departureCity = Integer.parseInt(departureCityAsString);
				flightNumber = Integer.parseInt(flightNumberAsString);
				arrivalCity = Integer.parseInt(arrivalCityAsString);

				airplaneType = airplaneTypeAsString;
				departureHour = departureHourAsString;
				arrivalHour = arrivalHourAsString;

				SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
				Date departureDate = sdf.parse(departureDateAsString);
				Date arrivalDate = sdf.parse(arrivalDateAsString);

				flight.setFlightNumber(flightNumber);
				flight.setUserID(userID);
				flight.setAirplaneType(airplaneType);
				flight.setArrivalCity(arrivalCity);
				flight.setDepartureCity(departureCity);
				flight.setDepartureDate(departureDate);
				flight.setArrivalDate(arrivalDate);
				flight.setDepartureHour(departureHour);
				flight.setArrivalHour(arrivalHour);

			} catch (ParseException e) {
				throw new ValidationException(e.getMessage());
			} catch (NumberFormatException e) {
				throw new ValidationException("Fields entered are not in required format!");
			}
		} else {
			throw new ValidationException("Toate campurile trebuie completate!");
		}

		return flight;
	}

}
