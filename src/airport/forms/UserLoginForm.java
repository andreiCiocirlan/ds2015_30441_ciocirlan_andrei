package airport.forms;

import javax.servlet.http.HttpServletRequest;

import airport.exception.ValidationException;
import airport.model.User;

public class UserLoginForm {
	private String usernameAsString;
	private String passwordAsString;

	public static UserLoginForm fromRequest(HttpServletRequest request) {

		UserLoginForm form = new UserLoginForm();

		form.usernameAsString = request.getParameter("username");
		form.passwordAsString = request.getParameter("password");

		return form;
	}

	public User toModel() throws ValidationException {

		User clientLoginModel = new User();

		String username = null;
		if (!usernameAsString.isEmpty()) {
			username = usernameAsString;
		} else {
			throw new ValidationException("Username trebuie completat!");
		}
		clientLoginModel.setUsername(username);

		String password = null;
		if (!passwordAsString.isEmpty()) {
			password = passwordAsString;

		} else {
			throw new ValidationException("Password trebuie completat!");
		}
		clientLoginModel.setPassword(password);

		return clientLoginModel;
	}

}
