package airport.DAO;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import airport.model.User;

public class UserDao {
	
	private static final Log LOGGER = LogFactory.getLog(UserDao.class);

	private SessionFactory factory;

	public UserDao(SessionFactory factory) {
		this.factory = factory;
	}

	/*public Student deleteStudent(Student student){
		int studentId = -1;
		System.out.println("Attemtping to delete student = "+student.getId());
		Session session = factory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.delete(student);
			//student.setId(studentId);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			LOGGER.error("", e);
		} finally {
			session.close();
		}
		return student;
	}
	
	
	public Student addStudent(Student student) {
		int studentId = -1;
		Session session = factory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			studentId = (Integer) session.save(student);
			student.setId(studentId);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			LOGGER.error("", e);
		} finally {
			session.close();
		}
		return student;
	}
	*/
	@SuppressWarnings("unchecked")
	public User getClientByUsername(String username) {
		Session session = factory.openSession();
		Transaction tx = null;
		List<User> users = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM User WHERE username = :username");
			query.setParameter("username", username);
			users = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			LOGGER.error("", e);
		} finally {
			session.close();
		}
		return users != null && !users.isEmpty() ? users.get(0) : null;
	}
	
	@SuppressWarnings("unchecked")
	public String checkUserPass(User clientLogin) {
		Session session = factory.openSession();
		Transaction tx = null;
		List<User> clients = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM User WHERE username = :username AND password = :password");
			query.setParameter("username", clientLogin.getUsername());
			query.setParameter("password", clientLogin.getPassword());
			clients = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			LOGGER.error("", e);
		} finally {
			session.close();
		}
		return clients != null && !clients.isEmpty() ? clients.get(0).getRole() : "incorrect";
	}
	
	
}
