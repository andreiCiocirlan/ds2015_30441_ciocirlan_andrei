package airport.DAO;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import airport.model.City;
import airport.model.Flight;

public class FlightDao {

	private static final Log LOGGER = LogFactory.getLog(UserDao.class);

	private SessionFactory factory;

	public FlightDao(SessionFactory factory) {
		this.factory = factory;
	}

	public void updateFlight(Flight flight){
		Session session = factory.openSession();
		Transaction tx = null;
		try{
	        tx = session.beginTransaction();
	        Flight f = (Flight) session.load(Flight.class, flight.getfFlightID());
	        f.setAirplaneType(flight.getAirplaneType());
	        f.setArrivalCity(flight.getArrivalCity());
	        f.setDepartureCity(flight.getDepartureCity());
	        f.setArrivalDate(flight.getArrivalDate());
	        f.setDepartureDate(flight.getDepartureDate());
	        f.setArrivalHour(flight.getArrivalHour());
	        f.setDepartureHour(flight.getDepartureHour());
	        session.update(f);
	        tx.commit();
        }catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			LOGGER.error("", e);
		} finally {
			session.close();
		}
       
	}
	
	public void deleteFlight(Flight flight){
		System.out.println("Attemtping to delete flight = "+flight.getFlightNumber());
		Session session = factory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.delete(flight);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			LOGGER.error("", e);
		} finally {
			session.close();
		}
	}
	
	@SuppressWarnings("unchecked")
	public Flight getFlightByNumber(int flightNumber){
		Session session = factory.openSession();
		Transaction tx = null;
		List<Flight> flights = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM Flight WHERE flightNumber = :flightNumber");
			query.setParameter("flightNumber", flightNumber);
			flights = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			LOGGER.error("", e);
		} finally {
			session.close();
		}
		return flights != null && !flights.isEmpty() ? flights.get(0) : null;
	}

	@SuppressWarnings("unchecked")
	public List<Flight> findFlights() {
		Session session = factory.openSession();
		Transaction tx = null;
		List<Flight> flights = null;
		try {
			tx = session.beginTransaction();
			flights = session.createQuery("FROM Flight").list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			LOGGER.error("", e);
		} finally {
			session.close();
		}
		return flights;
	}

	@SuppressWarnings("unchecked")
	public List<Flight> findFlightsForUserID(int userID) {
		Session session = factory.openSession();
		Transaction tx = null;
		List<Flight> flights = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM Flight WHERE userID = :userID");
			query.setParameter("userID", userID);
			flights = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			LOGGER.error("", e);
		} finally {
			session.close();
		}
		return flights;
	}
	
	@SuppressWarnings("unchecked")
	public String getCityByID(int cityID) {
		Session session = factory.openSession();
		Transaction tx = null;
		List<City> cities = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM City WHERE cityID = :cityID");
			query.setParameter("cityID", cityID);
			cities = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			LOGGER.error("", e);
		} finally {
			session.close();
		}
		return cities != null && !cities.isEmpty() ? cities.get(0).getCityName() : null;
	}
	
	
	public Flight addFlight(Flight flight) {
		int flightID = -1;
		Session session = factory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			flightID = (Integer) session.save(flight);
			System.out.println("Inserting flight : ID="+ flight.getfFlightID()+ " departureCityID=" +
				flight.getDepartureCity()+	" arrivalCityID=" + flight.getArrivalCity()+" arrivalHour="+
					flight.getArrivalHour() +" departureHour="+flight.getDepartureHour());
			flight.setfFlightID(flightID);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			LOGGER.error("", e);
		} finally {
			session.close();
		}
		return flight;
	}
	
}
