package airport.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.hibernate.cfg.Configuration;

import airport.DAO.UserDao;
import airport.exception.ValidationException;
import airport.forms.UserLoginForm;
import airport.model.User;

@SuppressWarnings("serial")
public class LoginServlet extends HttpServlet {
	
	private UserDao clientDao;
	
	public LoginServlet() {
		super();
		clientDao = new UserDao(new Configuration().configure().buildSessionFactory());
	}


	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		authenticate(request, response);
	}

	private void authenticate(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		UserLoginForm loginForm = UserLoginForm.fromRequest(request);
		
		try {
			User user = loginForm.toModel();
			if (clientDao.checkUserPass(user).equals("incorrect")) {
				HttpSession session = request.getSession(true);
				session.setAttribute("message", "Autentificare esuata!");
				goToLogin(request, response);
			}else if (clientDao.checkUserPass(user).equals("admin")) {
				User adminModel = clientDao.getClientByUsername(user.getUsername());
					String admin = adminModel.getLastname()+"(Administrator)";
					adminModel.setLastname(admin);
					HttpSession session = request.getSession();
					session.setAttribute("user", adminModel);
					session.setAttribute("userType", "admin");

					goToMainPage(request, response);
				}else if (clientDao.checkUserPass(user).equals("client")){
					User clientModel = clientDao.getClientByUsername(user.getUsername());

					HttpSession session = request.getSession();
					session.setAttribute("user", clientModel);
					session.setAttribute("userType", "client");
					
					goToMainPage(request, response);
				} 
		} catch (ValidationException ve) {
			HttpSession session = request.getSession(true);
			session.setAttribute("message", ve.getMessage());
			goToLogin(request, response);
		} 
	}

	private void goToMainPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("/mainHeader.do").forward(request,
				response);
	}

	private void goToLogin(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect(request.getContextPath() + "/login.jsp");
	}

}
