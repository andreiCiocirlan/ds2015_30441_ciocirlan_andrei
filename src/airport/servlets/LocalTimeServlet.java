package airport.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.cfg.Configuration;

import airport.DAO.CityDao;
import airport.model.City;

@SuppressWarnings("serial")
public class LocalTimeServlet extends HttpServlet{

	CityDao cityDao ;
	
	public LocalTimeServlet() {
		super();
		cityDao = new CityDao(new Configuration().configure().buildSessionFactory());
	}
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException{
		List<City> cities = new ArrayList<City>();
		cities = cityDao.findCities();
		if (cities != null && !cities.isEmpty()) {
			request.setAttribute("cities", cities);
		} else {
			request.setAttribute("message", "There are no cities !");
		}
		goToLocalTimePage(response, request);	
	}
	
	public void goToLocalTimePage(HttpServletResponse response, HttpServletRequest request) throws ServletException, IOException{
		request.getRequestDispatcher("/WEB-INF/JSP/localTime.jsp").forward(
				request, response);
	}

	
}
