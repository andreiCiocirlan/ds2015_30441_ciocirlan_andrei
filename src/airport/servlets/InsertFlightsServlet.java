package airport.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.cfg.Configuration;

import airport.DAO.CityDao;
import airport.DAO.FlightDao;
import airport.exception.ValidationException;
import airport.forms.FlightForm;
import airport.model.City;
import airport.model.Flight;

@SuppressWarnings("serial")
public class InsertFlightsServlet extends HttpServlet {

	FlightDao flightDao ;
	CityDao cityDao ;
	
	public InsertFlightsServlet() {
		super();
		flightDao = new FlightDao(new Configuration().configure().buildSessionFactory());
		cityDao = new CityDao(new Configuration().configure().buildSessionFactory());
	}
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException{
		List<City> cities = new ArrayList<City>();
		cities = cityDao.findCities();
		if (cities != null && !cities.isEmpty()) {
			request.setAttribute("cities", cities);
		} else {
			request.setAttribute("message", "There are no cities !");
		}
		goToInsertFlightsPage(response, request);	
	}
	
	public void goToInsertFlightsPage(HttpServletResponse response, HttpServletRequest request) throws ServletException, IOException{
		request.getRequestDispatcher("/WEB-INF/JSP/insertFlight.jsp").forward(
				request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		FlightForm f = FlightForm.fromRequest(request);
		Flight flight = null;
		try {
			flight = f.toModel();
			flightDao.addFlight(flight);
			request.setAttribute("message", "Flight added successfully !");
		} catch (ValidationException e) {
			request.setAttribute("message", e.getMessage());
		}
		goToInsertFlightsPage(response, request);
		
			
	}
}
