package airport.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.cfg.Configuration;

import airport.DAO.CityDao;
import airport.DAO.FlightDao;
import airport.exception.ValidationException;
import airport.forms.FlightForm;
import airport.model.City;
import airport.model.Flight;

@SuppressWarnings("serial")
public class UpdateFlightServlet  extends HttpServlet{
	FlightDao flightDao ;
	CityDao cityDao ;
	
	public UpdateFlightServlet() {
		super();
		flightDao = new FlightDao(new Configuration().configure().buildSessionFactory());
		cityDao = new CityDao(new Configuration().configure().buildSessionFactory());
	}
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException{
		
		try {
			List<Flight> flights = flightDao.findFlights();
			List<City> cities = new ArrayList<City>();
			cities = cityDao.findCities();
			
			if (cities != null && !cities.isEmpty()) {
				request.setAttribute("cities", cities);
			} else {
				request.setAttribute("message", "There are no cities !");
			}
			if (flights != null && !flights.isEmpty()) {
				for (int i=0;i<flights.size();i++){
					int departureCityID = flights.get(i).getDepartureCity();
					int arrivalCityID = flights.get(i).getArrivalCity();
					flights.get(i).setDepartureCityName(flightDao.getCityByID(departureCityID));
					flights.get(i).setArrivalCityName(flightDao.getCityByID(arrivalCityID));
				}
				request.setAttribute("flights", flights);
			}else{
				request.setAttribute("message", "There are no flights");
			}
			goToUpdateFlightsPage(response, request);	
		} catch (ServletException e) {
			request.setAttribute("message", e.getMessage());
		}
	}
	
	public void goToUpdateFlightsPage(HttpServletResponse response, HttpServletRequest request) throws ServletException, IOException{
		request.getRequestDispatcher("/WEB-INF/JSP/updateFlight.jsp").forward(
				request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		FlightForm f = FlightForm.fromRequest(request);
		Flight flight = null;
		try {
			flight = f.toModel();
			flight.setfFlightID(flightDao.getFlightByNumber(flight.getFlightNumber()).getfFlightID());
			flightDao.updateFlight(flight);
		} catch (ValidationException e) {
			request.setAttribute("message", e.getMessage());
		}
		goToUpdateFlightsPage(response, request);
	}
}
