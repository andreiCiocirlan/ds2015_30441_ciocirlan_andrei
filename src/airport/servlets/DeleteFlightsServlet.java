package airport.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.cfg.Configuration;

import airport.DAO.FlightDao;
import airport.model.Flight;

@SuppressWarnings("serial")
public class DeleteFlightsServlet extends HttpServlet{

	private FlightDao flightDao;

	public DeleteFlightsServlet() {
		super();
		flightDao = new FlightDao(new Configuration().configure().buildSessionFactory());
	}
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		getAllFlights(request,response);
	}
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String flightNumberAsString = request.getParameter("flightNumber");
		try{
			int flightNumber = Integer.parseInt(flightNumberAsString);
			if (flightDao.getFlightByNumber(flightNumber) != null){
				Flight flight = flightDao.getFlightByNumber(flightNumber);
				flightDao.deleteFlight(flight);
			}else{
				//HttpSession session = request.getSession(true);
				//session.setAttribute("message", "Flight number you entered cannot be found in the database !");
				request.setAttribute("message", "Flight number you entered cannot be found in the database !");
			}
		}catch(NumberFormatException e){
			//HttpSession session = request.getSession(true);
			//session.setAttribute("message", "Flight number format is invalid !");
			request.setAttribute("message", "Flight number format is invalid !");
		}
		
		getAllFlights(request,response);
	}
	
	public void getAllFlights(HttpServletRequest request, HttpServletResponse response) throws IOException {
		try {
			List<Flight> flights = flightDao.findFlights();
			
			if (flights != null && !flights.isEmpty()) {
				for (int i=0;i<flights.size();i++){
					int departureCityID = flights.get(i).getDepartureCity();
					int arrivalCityID = flights.get(i).getArrivalCity();
					flights.get(i).setDepartureCityName(flightDao.getCityByID(departureCityID));
					flights.get(i).setArrivalCityName(flightDao.getCityByID(arrivalCityID));
				}
				request.setAttribute("flights", flights);
			}else{
				request.setAttribute("message", "There are no flights");
			}
			request.getRequestDispatcher("/WEB-INF/JSP/deleteFlights.jsp").forward(
					request, response);
		} catch (ServletException e) {
			request.setAttribute("message", e.getMessage());
		}
	}
	
}
