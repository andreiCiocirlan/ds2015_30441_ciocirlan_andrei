package airport.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.hibernate.cfg.Configuration;

import airport.DAO.FlightDao;
import airport.model.Flight;
import airport.model.User;

@SuppressWarnings("serial")
public class ClientViewFlightsServlet extends HttpServlet{

	private FlightDao flightDao;

	public ClientViewFlightsServlet() {
		super();
		flightDao = new FlightDao(new Configuration().configure().buildSessionFactory());
	}
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		User user = new User();
		HttpSession session = request.getSession();
		
		user = (User) session.getAttribute("user");
		
		try {
			List<Flight> flights = flightDao.findFlightsForUserID(user.getUserID());
			
			if (flights != null && !flights.isEmpty()) {
				for (int i=0;i<flights.size();i++){
					int departureCityID = flights.get(i).getDepartureCity();
					int arrivalCityID = flights.get(i).getArrivalCity();
					flights.get(i).setDepartureCityName(flightDao.getCityByID(departureCityID));
					flights.get(i).setArrivalCityName(flightDao.getCityByID(arrivalCityID));
				}
				request.setAttribute("flights", flights);
			}else{
				request.setAttribute("message", "There are no flights");
			}
			request.getRequestDispatcher("/WEB-INF/JSP/visualizeFlights.jsp").forward(
					request, response);
		} catch (ServletException e) {
			request.setAttribute("message", e.getMessage());
			
		}
	}
	
}
