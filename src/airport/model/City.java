package airport.model;

public class City {

	private int cityID;
	private String cityName;
	private float cityLatitude;
	private float cityLongitude;
	
	public int getCityID() {
		return cityID;
	}
	public void setCityID(int cityID) {
		this.cityID = cityID;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public float getCityLatitude() {
		return cityLatitude;
	}
	public void setCityLatitude(float cityLatitude) {
		this.cityLatitude = cityLatitude;
	}
	public float getCityLongitude() {
		return cityLongitude;
	}
	public void setCityLongitude(float cityLongitude) {
		this.cityLongitude = cityLongitude;
	}

}