package airport.model;

import java.util.Date;

public class Flight {

	private int fFlightID;

	private int userID;
	private String airplaneType;
	private int flightNumber;
	private int departureCity;
	private Date departureDate;
	private String departureHour;
	private int arrivalCity;
	private Date arrivalDate;

	private String arrivalHour;
	private String departureCityName;
	private String arrivalCityName;

	public int getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(int flightNumber) {
		this.flightNumber = flightNumber;
	}

	public int getDepartureCity() {
		return departureCity;
	}

	public void setDepartureCity(int departureCity) {
		this.departureCity = departureCity;
	}

	public int getArrivalCity() {
		return arrivalCity;
	}

	public void setArrivalCity(int arrivalCity) {
		this.arrivalCity = arrivalCity;
	}

	public int getfFlightID() {
		return fFlightID;
	}

	public void setfFlightID(int fFlightID) {
		this.fFlightID = fFlightID;
	}

	public int getUserID() {
		return userID;
	}

	public void setUserID(int userID) {
		this.userID = userID;
	}

	public String getAirplaneType() {
		return airplaneType;
	}

	public void setAirplaneType(String airplaneType) {
		this.airplaneType = airplaneType;
	}

	public Date getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	public String getDepartureHour() {
		return departureHour;
	}

	public void setDepartureHour(String departureHour) {
		this.departureHour = departureHour;
	}

	public Date getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(Date arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	public String getArrivalHour() {
		return arrivalHour;
	}

	public void setArrivalHour(String arrivalHour) {
		this.arrivalHour = arrivalHour;
	}

	public String getDepartureCityName() {
		return departureCityName;
	}

	public void setDepartureCityName(String cityName) {
		this.departureCityName = cityName;
	}

	public String getArrivalCityName() {
		return arrivalCityName;
	}

	public void setArrivalCityName(String cityName) {
		this.arrivalCityName = cityName;
	}

}
